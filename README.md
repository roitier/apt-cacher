***Configuração do Servidor "Apt-Cacher" dos Laboratórios do IFGoiano Campus Ceres***

Para utilizá-lo nos laboratórios siga uma das opções: 


**Opção 1** - Adicione o seguinte conteúdo ao final do arquivo "/etc/apt/sources.list". 

    ```
    echo "deb http://IP_do_servidor:3142/debian buster main" > /etc/apt/sources.list
    ```

**Opção 2**
    Crie um arquivo "apt-cacher.list" no diretório /etc/apt/sources.list.d/
    
    ```
    echo "deb http://IP_do_servidor:3142/debian buster main" > /etc/apt/sources.list.d/apt-cacher.list
    ```

Ao final, atulize a lista de pacotes com o comendo 

    ```
    apt update
    ```

Pronto! Pode utilizar!


**ATENÇÂO:** Lembre-se de alterar o endereço do servidor no arquivo citado. 

